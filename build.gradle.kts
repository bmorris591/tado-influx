import de.undercouch.gradle.tasks.download.Download
import io.gitlab.arturbosch.detekt.Detekt
import nl.littlerobots.vcu.plugin.versionCatalogUpdate
import org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
import org.jetbrains.kotlin.gradle.tasks.KotlinJvmCompile
import org.openapitools.generator.gradle.plugin.tasks.GenerateTask

plugins {
    java
    application
    `project-report`
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.kotlin.spring)
    alias(libs.plugins.git.props)
    alias(libs.plugins.build.info)
    alias(libs.plugins.versions)
    alias(libs.plugins.catalog.update)

    alias(libs.plugins.jib)

    alias(libs.plugins.openapi.generator)

    alias(libs.plugins.download)

    alias(libs.plugins.spotless)
    alias(libs.plugins.detekt)

    alias(libs.plugins.java.agent.application)
    alias(libs.plugins.java.agent.jib)
}

repositories {
    mavenCentral()
    maven("https://repo.spring.io/milestone")
    maven("https://gitlab.com/api/v4/projects/10133668/packages/maven")
    maven("https://gitlab.com/api/v4/projects/26731120/packages/maven")
}

val mainClassKt = "uk.co.borismorris.tado.TadoInfluxApplicationKt"

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

tasks.withType<KotlinJvmCompile> {
    kotlinOptions {
        jvmTarget = "17"
        allWarningsAsErrors = true
        freeCompilerArgs = listOfNotNull(
            "-Xjsr305=strict",
            "-progressive",
            "-opt-in=kotlin.RequiresOptIn",
            "-opt-in=kotlin.ExperimentalUnsignedTypes",
            "-opt-in=kotlin.ExperimentalStdlibApi",
            "-opt-in=kotlinx.coroutines.FlowPreview",
            "-opt-in=kotlinx.coroutines.ExperimentalCoroutinesApi",
            "-opt-in=kotlin.time.ExperimentalTime"
        )
    }
    dependsOn("openApiGenerate")
}

configurations.all {
    exclude(module = "spring-boot-starter-logging")
    exclude(module = "spring-boot-starter-reactor-netty")
    exclude(module = "reactor-netty")
}

dependencies {
    implementation(platform(libs.spring.boot))
    implementation(platform(libs.otel))
    implementation(platform(libs.kotlinx.coroutines))
    implementation(platform(libs.log4j))

    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactive")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")

    implementation(libs.swagger.annotations)
    implementation(libs.jsr305)
    implementation(libs.jackson.nullable)
    implementation(libs.javax.annotations)

    implementation("org.springframework.security:spring-security-oauth2-client")

    implementation(libs.mapdb)

    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.boot:spring-boot-starter-log4j2")
    implementation("io.projectreactor:reactor-core")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("io.micrometer:micrometer-core")
    implementation("io.micrometer:micrometer-registry-influx")

    implementation("io.opentelemetry:opentelemetry-api")
    implementation("io.opentelemetry:opentelemetry-extension-kotlin")
    implementation(libs.otel.annotations)

    implementation(libs.reactive.flux)

    implementation(libs.kotlin.logging)
    implementation(libs.slack.appender)
    implementation(libs.httpclient.slack.client)
    runtimeOnly("org.apache.logging.log4j:log4j-slf4j2-impl")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("io.projectreactor:reactor-test")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testImplementation("org.junit.jupiter:junit-jupiter-params")
    testImplementation("org.assertj:assertj-core")
    testImplementation(libs.awaitility)
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test")
    testImplementation(libs.wiremock)
    testImplementation("org.mockito:mockito-junit-jupiter")
    testImplementation(libs.mockito.kotlin)
    testImplementation(kotlin("test-junit5"))

    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")

    javaagent(libs.otel.agent)
}

versionCatalogUpdate {
    pin {
        libraries.add(libs.swagger.annotations)
    }
}

tasks.withType<GenerateTask> {
    validateSpec.set(true)
    generatorName.set("java")
    library.set("webclient")
    outputDir.set("$buildDir/generated")
    inputSpec.set(file("src/main/swagger/com/tado/api_v2.yaml").toString())
    packageName.set("com.tado")
    apiPackage.set("com.tado.api")
    modelPackage.set("com.tado.model")
    generateApiTests.set(false)
    generateModelTests.set(false)
    configOptions.set(
        mapOf(
            "dateLibrary" to "java8",
            "sourceFolder" to "tado"
        )
    )
    typeMappings.set(
        mapOf(
            "float" to "BigDecimal",
            "double" to "BigDecimal"
        )
    )
    importMappings.set(
        mapOf(
            "BigDecimal" to "java.math.BigDecimal"
        )
    )
}

sourceSets.main {
    java.srcDirs("$buildDir/generated/tado")
}

spotless {
    kotlin {
        ktlint()
            .setUseExperimental(true)
    }
    kotlinGradle {
        ktlint()
            .setUseExperimental(true)
    }
}

val downloadDetektConfig by tasks.registering(Download::class) {
    src("https://gitlab.com/bmorris591/detekt/-/raw/main/detekt.yaml?inline=false")
    dest("$buildDir/detket.config")
    onlyIfModified(true)
    useETag("all")
}

detekt {
    buildUponDefaultConfig = true
    allRules = true
    config = files(downloadDetektConfig.get().dest)
}

tasks["check"].dependsOn("detektMain")

tasks.withType<Detekt> {
    jvmTarget = "17"
    dependsOn(downloadDetektConfig)
}

tasks.withType<Test> {
    useJUnitPlatform()

    testLogging {
        exceptionFormat = FULL
        showStandardStreams = true
        events("skipped", "failed")
    }
}

application {
    mainClass.set(mainClassKt)
    applicationDefaultJvmArgs = listOfNotNull(
        "-XX:+DisableAttachMechanism",
        "-Dcom.sun.management.jmxremote",
        "-Dcom.sun.management.jmxremote.port=9000",
        "-Dcom.sun.management.jmxremote.local.only=false",
        "-Dcom.sun.management.jmxremote.authenticate=false",
        "-Dcom.sun.management.jmxremote.ssl=false",
        "-Dcom.sun.management.jmxremote.rmi.port=9000",
        "-Dotel.service.name=${project.name}",
        "-Dotel.traces.exporter=logging,jaeger",
        "-Dotel.metrics.exporter=none",
        "-Dotel.propagators=tracecontext,baggage,b3",
        "-Djava.rmi.server.hostname=127.0.0.1"
    )
}

jib {
    from {
        image = "gcr.io/distroless/java17-debian11"
    }
    container {
        appRoot = "/opt/tado"
        workingDirectory = "/opt/tado"
        val javaToolOptions = listOfNotNull("-XX:InitialRAMPercentage=50", "-XX:MaxRAMPercentage=85").joinToString(separator = " ")
        environment = mapOf(
            "JAVA_TOOL_OPTIONS" to javaToolOptions,
            "SPRING_PROFILES_ACTIVE" to "prod",
            "OTEL_SERVICE_NAME" to project.name,
            "OTEL_PROPAGATORS" to "tracecontext,baggage,b3",
            "OTEL_TRACES_EXPORTER" to "logging",
            "OTEL_METRICS_EXPORTER" to "none"
        )
        jvmFlags = listOfNotNull("-XX:+PrintCommandLineFlags", "-XX:+DisableExplicitGC")
        mainClass = mainClassKt
        creationTime.set("USE_CURRENT_TIMESTAMP")
    }
}

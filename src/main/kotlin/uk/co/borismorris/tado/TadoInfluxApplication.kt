package uk.co.borismorris.tado

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.common.cache.CacheBuilder
import com.google.common.util.concurrent.ThreadFactoryBuilder
import com.tado.ApiClient
import com.tado.RFC3339DateFormat
import com.tado.api.HomeApi
import io.opentelemetry.api.GlobalOpenTelemetry
import io.opentelemetry.api.trace.Tracer
import kotlinx.coroutines.runBlocking
import mu.KLogging
import org.mapdb.DB
import org.mapdb.DBMaker
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectProvider
import org.springframework.beans.factory.getBean
import org.springframework.boot.SpringApplication
import org.springframework.boot.WebApplicationType
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.info.BuildProperties
import org.springframework.boot.info.GitProperties
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpHeaders.ETAG
import org.springframework.http.HttpHeaders.IF_NONE_MATCH
import org.springframework.http.client.reactive.JdkClientHttpConnector
import org.springframework.scheduling.concurrent.ThreadPoolExecutorFactoryBean
import org.springframework.security.oauth2.client.AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager
import org.springframework.security.oauth2.client.OAuth2AuthorizationContext.PASSWORD_ATTRIBUTE_NAME
import org.springframework.security.oauth2.client.OAuth2AuthorizationContext.USERNAME_ATTRIBUTE_NAME
import org.springframework.security.oauth2.client.ReactiveOAuth2AuthorizedClientManager
import org.springframework.security.oauth2.client.ReactiveOAuth2AuthorizedClientProvider
import org.springframework.security.oauth2.client.ReactiveOAuth2AuthorizedClientProviderBuilder
import org.springframework.security.oauth2.client.ReactiveOAuth2AuthorizedClientService
import org.springframework.security.oauth2.client.registration.ClientRegistration
import org.springframework.security.oauth2.client.registration.InMemoryReactiveClientRegistrationRepository
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository
import org.springframework.security.oauth2.client.web.reactive.function.client.ServerOAuth2AuthorizedClientExchangeFilterFunction
import org.springframework.security.oauth2.core.AuthorizationGrantType
import org.springframework.security.oauth2.core.ClientAuthenticationMethod
import org.springframework.web.reactive.function.client.ClientRequest
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.ExchangeFilterFunction
import org.springframework.web.reactive.function.client.ExchangeFunction
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers
import uk.co.borismorris.reactiveflux.InfluxClient
import uk.co.borismorris.reactiveflux.InfluxMeasurementConverter
import uk.co.borismorris.reactiveflux.conf.InfluxDbProps
import uk.co.borismorris.reactiveflux.webclient.WebclientInfluxClient
import uk.co.borismorris.tado.api.TadoApi
import uk.co.borismorris.tado.config.MapDbProps
import uk.co.borismorris.tado.config.TadoProps
import uk.co.borismorris.tado.influx.HomeStatePointConverter
import uk.co.borismorris.tado.logging.LogConfigOnStartup
import uk.co.borismorris.tado.logging.LogGitInfoOnStartup
import uk.co.borismorris.tado.logging.LogVersionOnStartup
import uk.co.borismorris.tado.oauth.PersistentMapOAuth2AuthorizedClientService
import uk.co.borismorris.tado.worker.TadoDataGatherer
import java.net.CookieManager
import java.net.URI
import java.net.http.HttpClient
import java.time.Duration
import java.util.TimeZone
import java.util.concurrent.Executor
import kotlin.system.exitProcess

private const val TADO = "tado"

@EnableAutoConfiguration
@Configuration(proxyBeanMethods = false)
@Suppress("TooManyFunctions")
class TadoInfluxApplication {

    @ConfigurationProperties(prefix = "tado")
    @Bean
    fun tadoProps() = TadoProps()

    @ConfigurationProperties(prefix = "mapdb")
    @Bean
    fun mapDpProps() = MapDbProps()

    @Bean
    fun cookieManager() = CookieManager()

    @Bean
    fun httpClientExecutor() = ThreadPoolExecutorFactoryBean().apply {
        setCorePoolSize(1)
        setQueueCapacity(1)
        setThreadFactory(ThreadFactoryBuilder().setNameFormat("http-client-%s").build())
    }

    @Bean
    fun httpClient(cookieManager: CookieManager, executor: Executor): HttpClient = HttpClient.newBuilder()
        .cookieHandler(cookieManager)
        .executor(executor)
        .build()

    @Bean
    fun httpConnector(httpClient: HttpClient) = JdkClientHttpConnector(httpClient)

    @Bean
    fun mapDp(config: MapDbProps) = DBMaker
        .fileDB(config.databaseLocation.toFile())
        .fileChannelEnable()
        .transactionEnable()
        .closeOnJvmShutdown()
        .make()

    @ConfigurationProperties(prefix = "influxdb")
    @Bean
    fun influxDbProps() = InfluxDbProps()

    @Bean
    fun homeStatePointConverter() = HomeStatePointConverter()

    @Bean
    fun influxClient(
        influxDbProps: InfluxDbProps,
        webclientBuilder: WebClient.Builder,
        converterProvider: ObjectProvider<InfluxMeasurementConverter>
    ) = WebclientInfluxClient(influxDbProps, webclientBuilder, converterProvider.toList())

    @Bean
    @Suppress("Deprecation")
    fun reactiveClientRegistrationRepository(tadoProps: TadoProps) = ClientRegistration.withRegistrationId(TADO)
        .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_POST)
        .clientId(tadoProps.oauth.clientId)
        .clientSecret(tadoProps.oauth.clientSecret)
        .authorizationGrantType(AuthorizationGrantType.PASSWORD)
        .tokenUri(tadoProps.oauth.tokenUrl.toString())
        .scope(tadoProps.oauth.scopes)
        .build()
        .let {
            InMemoryReactiveClientRegistrationRepository(it)
        }

    @Bean
    @Suppress("Deprecation")
    fun authorisedClientProvider(): ReactiveOAuth2AuthorizedClientProvider = ReactiveOAuth2AuthorizedClientProviderBuilder.builder()
        .refreshToken()
        .password()
        .build()

    @Bean
    fun reactiveOAuth2AuthorizedClientService(
        clientRegistrationRepository: ReactiveClientRegistrationRepository,
        db: DB
    ) = PersistentMapOAuth2AuthorizedClientService(clientRegistrationRepository, db)

    @Bean
    fun reactiveOAuth2AuthorizedClientManager(
        tadoProps: TadoProps,
        registrationRepository: ReactiveClientRegistrationRepository,
        authorizedClientService: ReactiveOAuth2AuthorizedClientService,
        authorizedClientProvider: ReactiveOAuth2AuthorizedClientProvider
    ): ReactiveOAuth2AuthorizedClientManager {
        val passwordAttributes = with(tadoProps.credentials) {
            mapOf(USERNAME_ATTRIBUTE_NAME to username, PASSWORD_ATTRIBUTE_NAME to password)
        }
        val attributesMapper = AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager.DefaultContextAttributesMapper().andThen { mono ->
            mono.map { attr -> (attr.asSequence() + passwordAttributes.asSequence()).associateBy({ it.key }, { it.value }) }
        }

        return AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager(registrationRepository, authorizedClientService).apply {
            setAuthorizedClientProvider(authorizedClientProvider)
            setContextAttributesMapper(attributesMapper)
        }
    }

    @Bean
    fun tadoApiClient(
        webclientBuilder: WebClient.Builder,
        objectMapper: ObjectMapper,
        authorizedClientManager: ReactiveOAuth2AuthorizedClientManager
    ) = ApiClient(
        webclientBuilder.filter(oauthFilter(authorizedClientManager)).filter(ETagFilterFunction()).build(),
        objectMapper,
        createDefaultDateFormat()
    )

    @Bean
    fun homeApi(apiClient: ApiClient) = HomeApi(apiClient)

    @Bean
    fun tadoApi(homeApi: HomeApi) = TadoApi(homeApi)

    @Bean
    fun tracer(): Tracer = GlobalOpenTelemetry.getTracer("tado-influx")

    @Bean
    fun tadoDataGatherer(tadoApi: TadoApi, influxClient: InfluxClient, tracer: Tracer) = TadoDataGatherer(tadoApi, influxClient, tracer)

    @Bean
    fun logConfigOnStartup(context: ConfigurableApplicationContext) = LogConfigOnStartup(context)

    @Bean
    fun logVersionOnStartup(buildProperties: BuildProperties) = LogVersionOnStartup(buildProperties)

    @Bean
    fun logGitInfoOnStartup(gitProperties: GitProperties) = LogGitInfoOnStartup(gitProperties)
}

@Suppress("TooGenericExceptionCaught", "SpreadOperator")
fun main(args: Array<String>) {
    @Suppress("DEPRECATION")
    Schedulers.enableMetrics()

    val context = SpringApplication(TadoInfluxApplication::class.java).apply {
        webApplicationType = WebApplicationType.NONE
    }.run(*args)

    try {
        runBlocking {
            context.startMonitoring()
        }
    } catch (e: Exception) {
        val logger = LoggerFactory.getLogger("uk.co.borismorris.netatmo.NetatmoInfluxApplicationKt")
        logger.error("Error during monitoring", e)
        exitProcess(1)
    }
}

suspend fun ConfigurableApplicationContext.startMonitoring() {
    val runner = getBean<TadoDataGatherer>()
    runner.run()
}

private fun createDefaultDateFormat() = RFC3339DateFormat().apply {
    timeZone = TimeZone.getTimeZone("UTC")
}

private fun oauthFilter(authorizedClientManager: ReactiveOAuth2AuthorizedClientManager) = ServerOAuth2AuthorizedClientExchangeFilterFunction(authorizedClientManager).also {
    it.setDefaultClientRegistrationId(TADO)
}

class ETagFilterFunction : ExchangeFilterFunction {

    companion object : KLogging()

    private val etags = CacheBuilder.newBuilder()
        .expireAfterWrite(Duration.ofMinutes(30))
        .removalListener<URI, String> { notification ->
            logger.info("Removed '{}':'{}' because of {}", notification.key, notification.value, notification.cause)
        }
        .build<URI, String>()

    override fun filter(request: ClientRequest, next: ExchangeFunction): Mono<ClientResponse> = Mono.just(request)
        .transformDeferredContextual { t, context ->
            context.getOrEmpty<String>(ETAG)
                .map { etag -> t.map { ClientRequest.from(it).header(IF_NONE_MATCH, etag).build() } }
                .orElse(t)
        }
        .flatMap { next.exchange(it) }
        .doOnNext { response ->
            response.headers().header(ETAG).firstOrNull()?.also { etag ->
                if (etags.getIfPresent(request.url()) != etag) {
                    etags.put(request.url(), etag)
                }
            }
        }
        .contextWrite { context -> etags.getIfPresent(request.url())?.let { context.put(ETAG, it) } ?: context }
}

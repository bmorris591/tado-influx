package uk.co.borismorris.tado.api

import com.tado.api.HomeApi
import com.tado.model.ControlDevice
import com.tado.model.TadoSystemType
import com.tado.model.UserHomesInner
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.reactive.asFlow
import com.tado.model.User as TadoUser
import com.tado.model.Zone as TadoZone

class TadoApi(private val homeApi: HomeApi) {

    fun user() = homeApi.showUser().asFlow().map { it.asUser() }

    private fun TadoUser.asUser() = User(this)

    @Suppress("UnsafeCallOnNullableType")
    inner class User(val user: TadoUser) {
        val name: String
            get() = user.name

        val id: String
            get() = user.id!!

        val email: String
            get() = user.email

        val username: String
            get() = user.username

        val locale: String?
            get() = user.locale

        fun homes() = user.homes!!.map { it.asHome() }

        override fun toString() = "User(user=$user)"

        fun UserHomesInner.asHome() = Home(this)

        inner class Home(val home: UserHomesInner) {
            val id: Long
                get() = home.id!!.toLong()

            val user: User
                get() = this@User

            fun info() = homeApi.showHome(id).asFlow()

            fun state() = homeApi.homeState(id).asFlow()

            fun devices() = homeApi.listMobileDevices(id).asFlow()

            fun zones() = homeApi.listZones(id).asFlow().map { it.asZone() }

            fun TadoZone.asZone() = Zone(this)

            override fun toString() = "Home(home=$home)"

            inner class Zone(val zone: TadoZone) {
                val id: Long
                    get() = zone.id.toLong()

                val name: String
                    get() = zone.name

                val type: TadoSystemType
                    get() = zone.type

                val devices: List<ControlDevice>
                    get() = zone.devices!!

                val home: Home
                    get() = this@Home

                fun capabilities() = homeApi.showZoneCapabilities(this@Home.id, this.id).asFlow()

                fun defaultOverlay() = homeApi.showZoneDefaultOverlay(this@Home.id, this.id).asFlow()

                fun overlay() = homeApi.showZoneOverlay(this@Home.id, this.id).asFlow()

                fun state() = homeApi.showZoneState(this@Home.id, this.id).asFlow()

                override fun toString() = "Zone(zone=$zone)"
            }
        }
    }
}

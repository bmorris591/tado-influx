@file:Suppress("UNNECESSARY_SAFE_CALL")

package uk.co.borismorris.tado.influx

import com.tado.model.HeatingZoneSetting
import com.tado.model.HomeState
import com.tado.model.MobileDevice
import com.tado.model.PercentageDataPoint
import com.tado.model.TemperatureDataPoint
import com.tado.model.ZoneState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import mu.KLogging
import uk.co.borismorris.reactiveflux.InfluxMeasurement
import uk.co.borismorris.reactiveflux.InfluxMeasurementConverter
import uk.co.borismorris.reactiveflux.measurement
import uk.co.borismorris.tado.api.TadoApi
import java.time.Instant

class HomeStatePointConverter : InfluxMeasurementConverter {
    companion object : KLogging()

    override fun convert(source: Any) = (source as TadoApi.User.Home).convert(Instant.now()).onEach {
        logger.info("Measurement: {}", it)
    }

    private fun TadoApi.User.Home.convert(now: Instant) = flow {
        fun MobileDevice.convertDevice() = measurement("device-state") {
            time(now)
            tag("home-id") to this@convert.id
            tag("device-id") to id
            tag("name") to name
            location?.apply {
                field("at-home") to atHome
                field("stale") to stale
            }
            settings?.apply {
                field("geo-tracking-enabled") to geoTrackingEnabled
            }
        }

        emitAll(convertHome(now))
        emitAll(devices().map { it.convertDevice() })
        emitAll(zones().flatMapConcat { it.convertZone(now) })
    }

    private fun TadoApi.User.Home.convertHome(now: Instant): Flow<InfluxMeasurement> {
        fun HomeState.measurement() = measurement("home-state") {
            time(now)
            tag("home-id") to id
            tag("name") to name
            field("presence") to presence
        }

        return state().map { it.measurement() }
    }

    private fun TadoApi.User.Home.Zone.convertZone(now: Instant): Flow<InfluxMeasurement> = flow {
        fun ZoneState.zoneMeasurement() = measurement("zone-state") {
            time(now)
            tag("home-id") to home.id
            tag("zone-id") to id
            tag("name") to name
            tag("type") to type

            field("zone-mode") to tadoMode
            field("window-open") to openWindowDetected

            link?.apply {
                field("link-state") to getState()
                reason?.apply {
                    field("link-reason-code") to code
                    field("link-reason-title") to title
                }
            }

            openWindow?.apply {
                field("open-window-total-duration") to durationInSeconds
                field("open-window-remaining-duration") to remainingTimeInSeconds
            }

            preparation?.apply {
                field("preparation-tado-mode") to tadoMode
                when (val setting = setting) {
                    is HeatingZoneSetting -> {
                        field("preparation-heating-state") to setting.power
                        setting.temperature?.apply {
                            field("preparation-target-temperature") to celsius
                        }
                    }
                }
            }

            when (val setting = setting) {
                is HeatingZoneSetting -> with(setting) {
                    field("heating-state") to power
                    temperature?.apply {
                        field("target-temperature") to celsius
                    }
                }
            }
        }

        fun PercentageDataPoint.measurement(measurement: String) = measurement(measurement) {
            time(timestamp.toInstant())
            tag("home-id") to home.id
            tag("zone-id") to id

            field("value") to percentage
        }

        fun TemperatureDataPoint.measurement(measurement: String) = measurement(measurement) {
            time(timestamp.toInstant())
            tag("home-id") to home.id
            tag("zone-id") to id

            field("value") to celsius
        }

        state().collect { state ->
            emit(state.zoneMeasurement())
            state.activityDataPoints?.heatingPower?.measurement("zone-heating-power")?.let { emit(it) }
            state.sensorDataPoints?.apply {
                insideTemperature?.measurement("zone-temperature")?.let { emit(it) }
                humidity?.measurement("zone-humidity")?.let { emit(it) }
            }
        }
    }

    override fun canConvert(source: Any) = source is TadoApi.User.Home
}

package uk.co.borismorris.tado.config

import java.net.URI

class TadoProps {
    val oauth = TadoOauthProps()
    val credentials = TadoCredentials()
    var retries: Long = 5

    override fun toString() = "NetamoProps(oauth=$oauth, credentials=$credentials)"
}

class TadoOauthProps {
    lateinit var tokenUrl: URI
    lateinit var clientId: String
    lateinit var clientSecret: String
    lateinit var scopes: Set<String>

    override fun toString() = "NetamoOauthProps(tokenUrl=$tokenUrl, clientId='$clientId', clientSecret='XXXX', scopes=$scopes)"
}

class TadoCredentials {
    lateinit var username: String
    lateinit var password: String

    override fun toString() = "NetamoCredentials(username='$username', password='XXXX')"
}

package uk.co.borismorris.tado.config

import java.nio.file.Path

class MapDbProps {
    lateinit var databaseLocation: Path

    override fun toString() = "MapDbProps(databaseLocation=$databaseLocation)"
}

package uk.co.borismorris.tado.oauth

import org.mapdb.DB
import org.mapdb.HTreeMap
import org.mapdb.Serializer
import org.springframework.security.core.Authentication
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientId
import org.springframework.security.oauth2.client.ReactiveOAuth2AuthorizedClientService
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository
import reactor.core.publisher.Mono
import java.io.Closeable

class PersistentMapOAuth2AuthorizedClientService(private val clientRegistrationRepository: ReactiveClientRegistrationRepository, private val db: DB) : ReactiveOAuth2AuthorizedClientService, Closeable {

    private val authorizedClients: HTreeMap<OAuth2AuthorizedClientId, OAuth2AuthorizedClient>

    init {
        @Suppress("UNCHECKED_CAST", "UnsafeCallOnNullableType")
        authorizedClients = db.hashMap(this::class.simpleName!!)
            .keySerializer(Serializer.JAVA)
            .valueSerializer(Serializer.JAVA)
            .createOrOpen() as HTreeMap<OAuth2AuthorizedClientId, OAuth2AuthorizedClient>
    }

    override fun <T : OAuth2AuthorizedClient?> loadAuthorizedClient(clientRegistrationId: String, principalName: String): Mono<T> = clientRegistrationRepository.findByRegistrationId(clientRegistrationId)
        .map { OAuth2AuthorizedClientId(clientRegistrationId, principalName) }
        .flatMap {
            @Suppress("UNCHECKED_CAST")
            Mono.justOrEmpty(authorizedClients[it] as T)
        }
        .checkpoint()

    @Suppress("ForbiddenVoid")
    override fun saveAuthorizedClient(authorizedClient: OAuth2AuthorizedClient, principal: Authentication): Mono<Void> = Mono.fromSupplier { OAuth2AuthorizedClientId(authorizedClient.clientRegistration.registrationId, principal.name) }
        .doOnNext {
            authorizedClients[it] = authorizedClient
            db.commit()
        }
        .checkpoint()
        .then()

    @Suppress("ForbiddenVoid")
    override fun removeAuthorizedClient(clientRegistrationId: String, principalName: String): Mono<Void> = clientRegistrationRepository.findByRegistrationId(clientRegistrationId)
        .map { OAuth2AuthorizedClientId(clientRegistrationId, principalName) }
        .doOnNext {
            authorizedClients.remove(it)
            db.commit()
        }
        .checkpoint()
        .then()

    override fun close() {
        authorizedClients.close()
    }
}

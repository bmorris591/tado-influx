package uk.co.borismorris.tado.worker

import io.opentelemetry.api.trace.Tracer
import io.opentelemetry.extension.kotlin.asContextElement
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.channels.TickerMode
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flatMapMerge
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import mu.KLogging
import uk.co.borismorris.reactiveflux.InfluxClient
import uk.co.borismorris.tado.api.TadoApi
import java.time.Duration

@OptIn(ObsoleteCoroutinesApi::class)
class TadoDataGatherer(private val tadoApi: TadoApi, private val influxClient: InfluxClient, private val tracer: Tracer) {

    companion object : KLogging()

    suspend fun run() = coroutineScope {
        ticker(Duration.ofDays(1).toMillis(), 0L).receiveAsFlow()
            .flatMapLatest { tadoApi.user() }
            .onEach { logger.info("Monitoring devices for user {}", it) }
            .flatMapConcat { it.homes().asFlow() }
            .onEach { logger.info("Monitoring devices for home {}", it) }
            .flatMapMerge { home -> ticker(Duration.ofMinutes(1).toMillis(), 0L, mode = TickerMode.FIXED_DELAY).receiveAsFlow().map { home } }
            .flatMapMerge { home ->
                val span = tracer.spanBuilder("poll-tado").startSpan()
                influxClient
                    .convertAndWrite(flowOf(home))
                    .flowOn(span.asContextElement())
                    .onCompletion { span.end() }
            }
            .collect { logger.trace("Sent item to Influx {}", it) }
    }
}
